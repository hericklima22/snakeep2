import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JPanel;

public class PainelGame extends JPanel implements Runnable, KeyListener {

	private static final long serialVersionUID = 1L;

	private static boolean Comum, Star, Kitty;

	private int DIFICULDADE;

	private static final int LARGURA = 600, ALTURA = 600;

	static Thread thread;
	
	static Thread threadPontos;

	private boolean running;

	private int vezesSimples = 1;

	private CorpoSnake b;
	private final ArrayList<CorpoSnake> snake;
	private CorpoStar star;
	private final ArrayList<CorpoStar> snakeStar;
	private CorpoKitty kitty;
	private final ArrayList<CorpoKitty> snakeKitty;

	private ComidaSimples comidaSimples;
	private ComidaBomba comidaBomba;
	private ComidaDecrease comidaDecrease;
	private BigFruit bigFruit;
	private final ArrayList<BigFruit> bigf;
	private final ArrayList<ComidaDecrease> comidad;
	private final ArrayList<ComidaSimples> comidas;
	private final ArrayList<ComidaBomba> comidab;

	private final Random r;

	private int x = (LARGURA / 2) / 10 - 1;
	private int y = (ALTURA / 2) / 10 - 1;
	private int tamanho = 5;
	private int tamanhoInicial = tamanho;

	private int ticks = 0;

	private boolean direita = false;
	private boolean esquerda = false;
	private boolean cima = false;
	private boolean baixo = true;

	private boolean bombaGerada, decreaseGerada,bigfruitGerada = false;

	private static int score = 0;
	private static int bestScore;

	public void setDificuldade(int DIFICULDADE) {
		this.DIFICULDADE = DIFICULDADE;
	}
	
	public void setPontos(int score) {
		bestScore = score;
	}

	public PainelGame(int cobrinhaSelecionada) {

		setFocusable(true);

		setPreferredSize(new Dimension(LARGURA, ALTURA));
		addKeyListener(this);

		if (cobrinhaSelecionada == 1) {
			Comum = true;
			Star = false;
			Kitty = false;
		}

		if (cobrinhaSelecionada == 2) {
			Comum = false;
			Star = true;
			Kitty = false;
		}

		if (cobrinhaSelecionada == 3) {
			Comum = false;
			Star = false;
			Kitty = true;
		}

		snake = new ArrayList<CorpoSnake>();
		snakeStar = new ArrayList<CorpoStar>();
		snakeKitty = new ArrayList<CorpoKitty>();
		comidas = new ArrayList<ComidaSimples>();
		comidab = new ArrayList<ComidaBomba>();
		comidad = new ArrayList<ComidaDecrease>();
		bigf = new ArrayList<BigFruit>();
		
		r = new Random();

		start();
	}

	public void start() {
		running = true;
		thread = new Thread(this);
		thread.start();
	}

	public void stop() {
		running = false;
	}

	/************************* DEBUG MODE *************************/
	private static boolean debugMode = false;
	int tickSet;

	/************************************************************/

	public void tick() {

		int tamanhoAnterior = tamanho;

		if (Comum) {
			if (snake.isEmpty()) {
				b = new CorpoSnake(x, y, 10);
				snake.remove(b);
			}
		}
		if (Star) {
			if (snakeStar.isEmpty()) {
				star = new CorpoStar(x, y, 10);
				snakeStar.remove(star);
			}
		}

		if (Kitty) {
			if (snakeKitty.isEmpty()) {
				kitty = new CorpoKitty(x, y, 10);
				snakeKitty.remove(kitty);
			}
		}

		ticks++;

		if (debugMode) {
			tickSet = 2000000;
		} else {
			tickSet = 200000;
		}

		if (ticks > tickSet) {
			if(tamanho == 0) {
				stop();
			}
			
			if (direita)
				x++;

			if (esquerda)
				x--;

			if (cima)
				y--;

			if (baixo)
				y++;

			if (debugMode) {
				System.out.println("x = " + x + " y = " + y);
			}

			ticks = 0;
			/**************** GERA COMUM ****************/

			if (Comum) {
				b = new CorpoSnake(x, y, 10);
				if (tamanhoAnterior > tamanho) {
					snake.remove(0);
				} else {
					snake.add(b);
				}

				if (snake.size() > tamanhoAnterior) {
					snake.remove(0);
				}
			}

			/**************** GERA STAR *****************/

			if (Star) {
				star = new CorpoStar(x, y, 10);
				if (tamanhoAnterior > tamanho) {
					snakeStar.remove(0);
				} else {
					snakeStar.add(star);
				}

				if (snakeStar.size() > tamanhoAnterior) {
					snakeStar.remove(0);
				}
			}

			/*************** GERA KITTY *****************/

			if (Kitty) {
				kitty = new CorpoKitty(x, y, 10);
				if (tamanhoAnterior > tamanho) {
					snakeKitty.remove(0);
				} else {
					snakeKitty.add(kitty);
				}

				if (snakeKitty.size() > tamanhoAnterior) {
					snakeKitty.remove(0);
				}
			}

			/******************************************/

			if (debugMode) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException ex) {
					Logger.getLogger(PainelGame.class.getName()).log(Level.SEVERE, null, ex);
				}
			} else {
				try {
					Thread.sleep(DIFICULDADE);
				} catch (InterruptedException ex) {
					Logger.getLogger(PainelGame.class.getName()).log(Level.SEVERE, null, ex);
				}
			}

		}
		/******************** BIGFRUIT ********************/
		
		if(bigfruitGerada) {
			if (bigf.isEmpty()) {
				int x = r.nextInt(LARGURA / 10 - 3);
				int y = r.nextInt(ALTURA / 10 - 3);
				
				if (x == 45 || x == 15) {
					x = r.nextInt();
				}
				
				bigFruit = new BigFruit(x, y, 10);
				bigf.add(bigFruit);
				
			}
		}

			for (int i = 0; i < bigf.size(); i++) {
				if (x == bigf.get(i).getX() && y == bigf.get(i).getY()) {
					bigf.clear();
				
					if (!Star) {
						score+=2;
					} else {
						score += 4;		
					}
				
					threadPontos = new Thread() {
						public void run() {
						bestScore = score;
						}
					};
					threadPontos.start();
					bigfruitGerada = false;
				}
			}
		
		/******************** BIGFRUIT ********************/
		/******************** BOMBFRUIT ********************/
		if (bombaGerada) {
			// gerador de comida BOMBA
			if (comidab.isEmpty()) {
				int x = r.nextInt(LARGURA / 10 - 3);
				int y = r.nextInt(ALTURA / 10 - 3);
				
				if (x == 45 || x == 15) {
					x = r.nextInt();
				}
				
				comidaBomba = new ComidaBomba(x, y, 10);
				comidab.add(comidaBomba);
			}
		}
		
		// comeu a comida BOMBA
		for (int i = 0; i < comidab.size(); i++) {
			if (x == comidab.get(i).getX() && y == comidab.get(i).getY()) {
				comidab.clear();
				stop();
			}
		}
		
		/******************** BOMBFRUIT ********************/
		
		/******************** DECREASEFRUIT ********************/
		if(decreaseGerada) {
			if (comidad.isEmpty()) {
				int x = r.nextInt(LARGURA / 10 - 3);
				int y = r.nextInt(ALTURA / 10 - 3);
				
				if (x == 45 || x == 15) {
					x = r.nextInt();
				}
				
				comidaDecrease = new ComidaDecrease(x, y, 10);
				comidad.add(comidaDecrease);
			}
		}

			for (int i = 0; i < comidad.size(); i++) {
				if (x == comidad.get(i).getX() && y == comidad.get(i).getY()) {
					comidad.clear();
					if(Comum) {
						snake.clear();
						tamanho = tamanhoInicial;
					}
					if(Star) {
						snakeStar.clear();
						tamanho = tamanhoInicial;
					}
					if(Kitty) {
						snakeKitty.clear();
						tamanho = tamanhoInicial;
					}
				}
			}
		
		/******************** DECREASEFRUIT ********************/
		/******************** SIMPLEFRUIT ********************/
			
		if(!bigfruitGerada) {
		// gerador de comida SIMPLES
		if (comidas.isEmpty()) {
			int x = r.nextInt(59);
			int y = r.nextInt(59);
			
			if(bombaGerada) { //bomba
				if(x == comidaBomba.getX()) {
					x = r.nextInt(59);
				}
				
				if(y == comidaBomba.getY()) {
					y = r.nextInt(59);
				}
			}
			
			else if(decreaseGerada) { //decrease
				if(x == comidaDecrease.getX()) {
					x = r.nextInt(59);
				}
				
				if(y == comidaDecrease.getY()) {
					y = r.nextInt(59);
				}
			}
			
			if (x == 45 || x == 15) {
				x = r.nextInt(59);
			}
			
			
			
			
			comidaSimples = new ComidaSimples(x, y, 10);
			comidas.add(comidaSimples);			
		}
			// comeu a comida SIMPLES
			for (int i = 0; i < comidas.size(); i++) {
				if (x == comidas.get(i).getX() && y == comidas.get(i).getY()) {
					if (bombaGerada) {
						comidab.clear();
					}
					if(decreaseGerada) {
						comidad.clear();
					}
					if(bigfruitGerada) {
						bigf.clear();
					}
					
					vezesSimples++;
					if(vezesSimples % 8 == 0) bigfruitGerada = true;
					else bigfruitGerada = false;
					
					if(vezesSimples % 5 == 0) decreaseGerada = true;
					else decreaseGerada = false;
					
					if(vezesSimples % 3 == 0) bombaGerada = true;
					else bombaGerada = false;
					
					tamanho++;
					comidas.remove(i);
					i++;
					

					if (!Star) {
						score++;
					} else {
						score += 2;		
					}
					
					threadPontos = new Thread() {
						public void run() {
							bestScore = score;
						}
					};
					threadPontos.start();
				}
			}
			}
			
			/******************** SIMPLEFRUIT ********************/			

		/************************ COLIS�O DE BORDA ************************/
		if (!Kitty) {
			if (x < 0 || x > 59 || y < 0 || y > 59) {
				stop();
			}
			
			if((y >= 15 && y <= 35) && (x == 15 || x == 45)) {
				stop();
			}
			
		} else {
			if (x < 0 || x > 59 || y < 0 || y > 59) {
				stop();
			}
		}
		/**************************************************************/

		/****************** COLISAO COMUM ******************/

		if (Comum) {
			for (int i = 0; i < snake.size(); i++) {
				if (x == snake.get(i).getX() && y == snake.get(i).getY()) {
					if (i != snake.size() - 1) {
						stop();
					}
				}
			}
		}

		/****************** COLISAO STAR *******************/

		if (Star) {
			for (int i = 0; i < snakeStar.size(); i++) {
				if (x == snakeStar.get(i).getX() && y == snakeStar.get(i).getY()) {
					if (i != snakeStar.size() - 1) {
						stop();
					}
				}
			}
		}

		/****************** COLISAO KITTY ******************/

		if (Kitty) {
			for (int i = 0; i < snakeKitty.size(); i++) {
				if (x == snakeKitty.get(i).getX() && y == snakeKitty.get(i).getY()) {
					if (i != snakeKitty.size() - 1) {
						stop();
					}
				}
			}
		}

		/*************************************************/

		repaint();

		if (!running) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@Override
	public void paint(Graphics g) {

		g.clearRect(0, 0, LARGURA, ALTURA);

		g.setColor(Color.CYAN);
		g.fillRect(0, 0, LARGURA, ALTURA);
		
		// paredes
		for (int i = 0; i < 200; i++) {
			g.setColor(Color.GRAY);
			g.fillRect(LARGURA / 4, 350 - i, 10, 10);
			g.fillRect((LARGURA / 4) + (LARGURA / 2), 350 - i, 10, 10);
		}

		if (Comum) {
			for (int i = 0; i < snake.size(); i++) {
				snake.get(i).draw(g);
			}
		}

		if (Star) {
			for (int i = 0; i < snakeStar.size(); i++) {
				snakeStar.get(i).draw(g);
			}
		}

		if (Kitty) {
			for (int i = 0; i < snakeKitty.size(); i++) {
				snakeKitty.get(i).draw(g);
			}
		}

		//desenha decrasefruit
		for (int i = 0; i < comidad.size(); i++) {
			comidad.get(i).draw(g);
		}
		
		//desenha bigfruit
		for (int i = 0; i < bigf.size(); i++) {
			bigf.get(i).draw(g);
		}
		
		// desenha comida SIMPLES
		for (int i = 0; i < comidas.size(); i++) {
			comidas.get(i).draw(g);
		}

		// desenha comida BOMBA
		for (int i = 0; i < comidab.size(); i++) {
			comidab.get(i).draw(g);
		}

		// desenha score
		if (bestScore >= 0) {
			if(running) {
				g.setColor(Color.YELLOW);
				g.setFont(new Font("Unispace", 8, 50));
				g.drawString(String.valueOf(bestScore), 30 - 15, 50);
			}	
		}

		

		if (!running) {
			g.setColor(Color.BLACK);
			g.setFont(new Font("Unispace", 8, 50));
			g.drawString(String.valueOf("GAME OVER"), (LARGURA) / 4 + 16, (ALTURA) / 3);

			g.setColor(Color.RED);
			g.setFont(new Font("Arial", 8, 50));
			g.drawString(String.valueOf("Pontua��o Final: " + bestScore), (LARGURA) / 8, (ALTURA) / 2 + 120);
		}

	}

	@Override
	public void run() {
		while (running) {
			tick();
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();

		if (key == KeyEvent.VK_RIGHT && !esquerda) {
			direita = true;
			cima = false;
			baixo = false;
		}

		if (key == KeyEvent.VK_LEFT && !direita) {
			esquerda = true;
			cima = false;
			baixo = false;
		}

		if (key == KeyEvent.VK_UP && !baixo) {
			direita = false;
			cima = true;
			esquerda = false;
		}

		if (key == KeyEvent.VK_DOWN && !cima) {
			direita = false;
			esquerda = false;
			baixo = true;
		}
		
		if(key == KeyEvent.VK_K) {
			Kitty = true;
			Comum = false;
			Star = false;
		}
		
		if(key == KeyEvent.VK_C) {
			Comum = true;
			Kitty = false;
			Star = false;
		}
		
		if(key == KeyEvent.VK_S) {
			Star = true;
			Kitty = false;
			Comum = false;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
