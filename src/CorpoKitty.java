import java.awt.Color;
import java.awt.Graphics;

public class CorpoKitty {
	
	private int x, y, altura, largura;

	public CorpoKitty(int x, int y, int tamanhoCauda) {
		this.x = x;
		this.y = y;
		altura = tamanhoCauda;
		largura = tamanhoCauda;
	}

	public void draw(Graphics g) {
			g.setColor(Color.PINK);
			g.fillRect(x * largura, y * altura, largura, altura);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
}
